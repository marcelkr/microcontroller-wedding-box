#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>
#include "morse.h"
#include "timer.h"
#include "config.h"


#define LED_ON(x)     (LED_PORT |= (1<<x))
#define LED_OFF(x)    (LED_PORT &= ~(1<<x))


/* ######################################################################### */
/* ### Typedefs */
typedef enum {
    LEVEL1,
    LEVEL2,
    LEVEL3,
    LEVEL4,
    LEVEL5,
    FINISHED
} Level_t;

typedef enum {
    OFF_LONG,
    OFF_SHORT,
    ON
} LedState_t;

typedef enum LockSt_e{
    LOCKED,
    UNLOCKED
} LockSt_t;


/* ######################################################################### */
/* ### Prototypes */
char readKey(void);
char* processKeyboardInput(void);
void changeLevel(Level_t newLvl);
void processBeep(void);
void beep(uint16_t time);


/* ######################################################################### */
/* ### Globals */
static char lvl3MorseString[] = LVL3_MORSE_STRING;
static char lvl4MorseString[] = LVL4_MORSE_STRING;

uint8_t eeLevel EEMEM = LEVEL1;
Level_t level = LEVEL1;
LockSt_t lockState = LOCKED;
soft_timer_t beepTmr;


/* ######################################################################### */
ISR(TIMER1_COMPB_vect) {
    static uint8_t phase = 1;

    if (phase == 1) {
        if (lockState == LOCKED) {
            OCR1A = SERVO_LOCKED_TIMER_VAL;
        } else {
            OCR1A = SERVO_UNLOCKED_TIMER_VAL;
        }
        phase = 2;
    } else {
        if (lockState == LOCKED) {
            OCR1A = T1_LOAD_VAL - SERVO_LOCKED_TIMER_VAL;
        } else {
            OCR1A = T1_LOAD_VAL - SERVO_UNLOCKED_TIMER_VAL;
        }
        phase = 1;
    }

    OCR1A = T1_LOAD_VAL - OCR1A;
}

ISR(TIMER0_COMPA_vect) {
    timer_counter++;
}


void processBeep(void) {
    if (timer_is_running(&beepTmr) && timer_is_expired(&beepTmr)) {
        BUZZER_PORT &= ~(1<<BUZZER_PIN);
        timer_stop(&beepTmr);
    }
}

void beep(uint16_t time) {
    BUZZER_PORT |= (1<<BUZZER_PIN);
    timer_start(&beepTmr, time);
}

void changeLevel(Level_t newLvl) {
    level = newLvl;
    eeprom_write_byte(&eeLevel, level);
}

char* processKeyboardInput(void) {
    static uint8_t i = 0;
    static char string[KB_MAX_INP_LENGTH];
    static soft_timer_t kbInputTimer;
    static soft_timer_t kbLedTimer;

    char key = readKey();

    if (key != KB_NOKEY && i < KB_MAX_INP_LENGTH) {
        timer_start(&kbInputTimer, KB_INPUT_TIMEOUT);     // Reset input timer
        string[i] = key;
        string[i+1] = '\0';
        i++;
    }

    if (timer_is_expired(&kbInputTimer) && timer_is_running(&kbInputTimer)) {
        // reset string
        timer_stop(&kbInputTimer);
        string[0] = '\0';
        i = 0;

        // turn on LED for a short while
        timer_start(&kbLedTimer, 100);
        LED_PORT |= (1<<LED_KB);
    }

    if (timer_is_expired(&kbLedTimer) && timer_is_running(&kbLedTimer)) {
        timer_stop(&kbLedTimer);
        LED_PORT &= ~(1<<LED_KB);
    }

    return string;
}

char readKey(void) {
    static soft_timer_t debounceTimer;
    static char oldKey = KB_NOKEY;
    char key = KB_NOKEY;
    char ret = KB_NOKEY;

    KB_COL1_PORT &= ~(1<<KB_COL1);
    _delay_us(200);   // Give the pin a while to go low // TODO minimize timing

    if (!(KB_ROW1_PIN & (1<<KB_ROW1))) {
        key = '1';
    }

    if (!(KB_ROW2_PIN & (1<<KB_ROW2))) {
        key = '4';
    }

    if (!(KB_ROW3_PIN & (1<<KB_ROW3))) {
        key = '7';
    }

    if (!(KB_ROW4_PIN & (1<<KB_ROW4))) {
        key = '*';
    }

    KB_COL1_PORT |= (1<<KB_COL1);
    KB_COL2_PORT &= ~(1<<KB_COL2);
    _delay_us(1);   // Give the pin a while to go low

    if (!(KB_ROW1_PIN & (1<<KB_ROW1))) {
        key = '2';
    }

    if (!(KB_ROW2_PIN & (1<<KB_ROW2))) {
        key = '5';
    }

    if (!(KB_ROW3_PIN & (1<<KB_ROW3))) {
        key = '8';
    }

    if (!(KB_ROW4_PIN & (1<<KB_ROW4))) {
        key = '0';
    }

    KB_COL2_PORT |= (1<<KB_COL2);
    KB_COL3_PORT &= ~(1<<KB_COL3);
    _delay_us(1);

    if (!(KB_ROW1_PIN & (1<<KB_ROW1))) {
        key = '3';
    }

    if (!(KB_ROW2_PIN & (1<<KB_ROW2))) {
        key = '6';
    }

    if (!(KB_ROW3_PIN & (1<<KB_ROW3))) {
        key = '9';
    }

    if (!(KB_ROW4_PIN & (1<<KB_ROW4))) {
        key = '#';
    }

    KB_COL3_PORT |= (1<<KB_COL3);

    if (timer_is_expired(&debounceTimer) || !timer_is_running(&debounceTimer)) {
        if ((oldKey != KB_NOKEY) && (key == KB_NOKEY)) {
            timer_start(&debounceTimer, KB_DEBOUNCE_TIME);
            ret = oldKey;
        } else {
            ret = KB_NOKEY;
        }
    } else {
        ret = KB_NOKEY;
    }

    oldKey = key;
    return ret;
}


void processLvlLed(void) {
    static uint8_t onTimes = 0;
    static LedState_t ledState = OFF_LONG;
    static soft_timer_t lvlLedTimer;

    if (!timer_is_running(&lvlLedTimer)) {
        timer_start(&lvlLedTimer, LVL_LED_OFF_LONG_TIME);
    }

    switch (ledState) {
        case OFF_LONG:
            if (timer_is_expired(&lvlLedTimer)) {
                LED_ON(LED_LVL);
                timer_start(&lvlLedTimer, LVL_LED_ON_TIME);
                ledState = ON;
            }
            break;

        case ON:
            if (timer_is_expired(&lvlLedTimer)) {
                LED_OFF(LED_LVL);

                if (level == FINISHED) {    // if finished, blink constantly
                    ledState = OFF_SHORT;
                    timer_start(&lvlLedTimer, LVL_LED_OFF_SHORT_TIME);
                    onTimes++;
                } else {    // if not yet finished, show the level via the blinking
                    if (onTimes >= level) {
                        ledState = OFF_LONG;
                        timer_start(&lvlLedTimer, LVL_LED_OFF_LONG_TIME);
                        onTimes = 0;
                    } else {
                        ledState = OFF_SHORT;
                        timer_start(&lvlLedTimer, LVL_LED_OFF_SHORT_TIME);
                        onTimes++;
                    }
                }
            }
            break;

        case OFF_SHORT:
            if (timer_is_expired(&lvlLedTimer)) {
                LED_ON(LED_LVL);
                timer_start(&lvlLedTimer, LVL_LED_ON_TIME);
                ledState = ON;
            }
            break;
    }
}

void lock(LockSt_t lockSt) {
    if (lockSt == LOCKED) {
        lockState = LOCKED;
    } else {
        lockState = UNLOCKED;
    }
}

void processLock(void) {
    static soft_timer_t lockTimer;
    static LockSt_t lockStateOld = UNLOCKED;

    if (lockState != lockStateOld) {
        timer_start(&lockTimer, 300);
        //TCCR1B |= (1<<CS10) | (1<<CS11);  // CTC mode; Prescaler of 64
        DDRB |= (1<<SERVO_PIN);
        lockStateOld = lockState;
    }

    if (timer_is_expired(&lockTimer) && timer_is_running(&lockTimer)) {
        //TCCR1B &= ~((1<<CS10) | (1<<CS11));   // Timer off
        timer_stop(&lockTimer);
        DDRB &= ~(1<<SERVO_PIN);
    }
}

void init(void) {
    DDRB |= (1 << PB2) | (1 << PB3) | (1<<PB5);
    DDRB &= ~(1 << PB1);
    DDRC |= (1 << PC0) | (1<<PC1);

    // nails
    NAIL_PORT |= (1<<NAIL1_PIN) | (1<<NAIL2_PIN);

    // Keyboard
    DDRD |= (1<<PD3) | (1<<PD5); // Keyboard column pins as outpts
    DDRB |= (1<<PB5); // Keyboard column pins as outpts

    DDRD &= ~((1<<PD1) | (1<<PD4) | (1<<PD6)); // Keyboard row pins as inputs
    PORTD |= (1<<PD1) | (1<<PD4) | (1<<PD6); // Activate internal pullups for keyboard row pins
    DDRB &= ~(1<<PB4); // Keyboard row pins as inputs
    PORTB |= (1<<PB4); // Activate internal pullups for keyboard row pins
    DDRB &= ~(1<<PB0); // Keyboard row pins as inputs
    PORTB |= (1<<PB0); // Activate internal pullups for keyboard row pins

    KB_COL1_PORT |= (1<<KB_COL1) | (1<<KB_COL3); // Columns high since they're low active
    PORTB |= (1<<PB5); // Columns high since they're low active

    BUZZER_PORT &= ~(1<<BUZZER_PIN);

    // Timers
	TCCR0A |= (1<<WGM01);              // CTC mode
    TCCR0B |= (1<<CS00) | (1<<CS02);   // Prescaler = 1024
    TIMSK0 |= (1<<OCIE0A);             // Enable interrupt
	OCR0A = 156;                       // Counter top value of 156 roughly translates to 10ms

    TCCR1A |= (1<<COM1B0);                         // Toggle at compare match
    TCCR1B |= (1<<WGM12) | (1<<CS10) | (1<<CS11);  // CTC mode; Prescaler of 64
    TIMSK1 |= (1<<OCIE1B);                         // Timer-Compare Interrupt an
    OCR1A = SERVO_LOCKED_TIMER_VAL;

    level = eeprom_read_byte(&eeLevel);

    // in case anything is strange in the EEPROM fall back to lvl1
    if (level > FINISHED) {
        changeLevel(LEVEL1);
        eeprom_write_byte(&eeLevel, level);
    }

    sei();

    lock(LOCKED);
}

int main(void)
{
    static soft_timer_t debounceTmr;
    char *userInput;
    init();
    beep(50);

    timer_start(&debounceTmr, 210);

    while(1) {
        morse_process();
        processBeep();
        processLvlLed();
        processLock();

        userInput = processKeyboardInput();

        if        (strcmp_P(userInput, PSTR(CHEATCODE_LVL1)) == 0) {
            changeLevel(LEVEL1);
        } else if (strcmp_P(userInput, PSTR(CHEATCODE_LVL2)) == 0) {
            changeLevel(LEVEL2);
        } else if (strcmp_P(userInput, PSTR(CHEATCODE_LVL3)) == 0) {
            changeLevel(LEVEL3);
        } else if (strcmp_P(userInput, PSTR(CHEATCODE_LVL4)) == 0) {
            changeLevel(LEVEL4);
        } else if (strcmp_P(userInput, PSTR(CHEATCODE_LVL5)) == 0) {
            changeLevel(LEVEL5);
        } else if (strcmp_P(userInput, PSTR(CHEATCODE_FINISH)) == 0) {
            changeLevel(FINISHED);
        } else if (strcmp_P(userInput, PSTR(CHEATCODE_LOCK)) == 0) {
            lock(LOCKED);
        } else if (strcmp_P(userInput, PSTR(CHEATCODE_UNLOCK)) == 0) {
            lock(UNLOCKED);
        }

        switch(level) {
            case LEVEL1:
                if(strcmp_P(userInput, PSTR(LVL1_SOLUTION_STRING)) == 0) {
                    beep(100);
                    changeLevel(LEVEL2);
                }
                break;

            case LEVEL2:
                if(strcmp_P(userInput, PSTR(LVL2_SOLUTION_STRING)) == 0) {
                    morse_string(lvl3MorseString);
                    changeLevel(LEVEL3);
                }
                break;

            case LEVEL3:
                // Repeat the morse code on demand
                if(strcmp_P(userInput, PSTR("*")) == 0 && timer_is_expired(&debounceTmr)) {
                    timer_start(&debounceTmr, 210);
                    morse_string(lvl3MorseString);
                }

                // change level
                if(strcmp_P(userInput, PSTR(LVL3_SOLUTION_STRING)) == 0) {
                    morse_string(lvl4MorseString);
                    changeLevel(LEVEL4);
                }
                break;

            case LEVEL4:
                // Repeat the morse code on demand
                if(strcmp_P(userInput, PSTR("*")) == 0 && timer_is_expired(&debounceTmr)) {
                    timer_start(&debounceTmr, 210);
                    morse_string(lvl4MorseString);
                }

                // change level
                if (!(NAIL_PIN & (1<<NAIL1_PIN)) && !(NAIL_PIN & (1<<NAIL2_PIN))) {
                    beep(100);
                    changeLevel(LEVEL5);
                }
                break;

            case LEVEL5:
                if (PINB & (1<<REED_PIN)) {
                    beep(100);
                    changeLevel(FINISHED);
                }
                break;

            case FINISHED:
                lock(UNLOCKED);
                break;
        }
    }

	return 0;
}
