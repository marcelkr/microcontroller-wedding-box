#include "morse.h"
#include "timer.h"
#include "config.h"

#include <avr/io.h>
#include <stdint.h>
#include <stdbool.h>

#define IS_DAH(x)	(0x01 & x)



#define LED_ON(x)     (LED_PORT |= (1<<x))
#define LED_OFF(x)    (LED_PORT &= ~(1<<x))

typedef enum {
	IDLE,
	BEEP,
	PAUSE
} MorseState_t;

/* Ascii to morse translation table
 * Attention: This table is offset by 32 to exclude the non printable characters!*/
static const uint8_t ascii_to_morse[] = { // not all official morse characters are implemented
	0xFF,		// space
	0,			// !
	0,			// "
	0,			// #
	0,			// $
	0,			// %
	0,			// &
	0b01011110,	// '
	0b00101101,	// (
	0b01101101, // )
	0,			// *
	0b00101010,	// +
	0b01110011,	// ,
	0b01100001,	// -
	0b01101010,	// .
	0b00101001,	// /
	0b00111111,	// 0
	0b00111110,	// 1
	0b00111100,	// 2
	0b00111000,	// 3
	0b00110000,	// 4
	0b00100000,	// 5
	0b00100001,	// 6
	0b00100011,	// 7
	0b00100111,	// 8
	0b00101111,	// 9
	0b01000111,	// :
	0b01010101,	// ;
	0,			// <
	0b00110001,	// =
	0,			// >
	0b01001100,	// ?
	0b01010110,	// @
	0b00000110,	// A	// uppercase letters hace the same morse code as lowercase ones
	0b00010001,	// B
	0b00010101, // ...
	0b00001001,
	0b00000010,
	0b00010100,
	0b00001011,
	0b00010000,
	0b00000100,
	0b00011110,
	0b00001101,
	0b00010010,
	0b00000111,
	0b00000101,
	0b00001111,
	0b00010110,
	0b00011011,
	0b00001010,
	0b00001000,
	0b00000011,
	0b00001100,
	0b00011000,
	0b00001110,
	0b00011001,
	0b00011101,	// ...
	0b00010011,	// Z
	0,			// [
	0,			// ]
	0,			/* \ */
	0,			// ^
	0,			// _
	0,			// `
	0b00000110,	// a
	0b00010001,	// b
	0b00010101, // ...
	0b00001001,
	0b00000010,
	0b00010100,
	0b00001011,
	0b00010000,
	0b00000100,
	0b00011110,
	0b00001101,
	0b00010010,
	0b00000111,
	0b00000101,
	0b00001111,
	0b00010110,
	0b00011011,
	0b00001010,
	0b00001000,
	0b00000011,
	0b00001100,
	0b00011000,
	0b00001110,
	0b00011001,
	0b00011101,	// ...
	0b00010011	// z
};

static soft_timer_t tmr;
static char *string;
static MorseState_t state = IDLE;
static bool start = false;

uint8_t getMorseCode (const char letter) {
	return *(ascii_to_morse + (uint8_t)letter - 32);
}

void morse_string(char* string_p)
{
	string = string_p;
	start = true;
}

void morse_process(void)
{
	static uint8_t i = 0;
	static uint8_t morse_symbol;

	switch (state) {
		case IDLE:
			if (start == true) {	// start condition
				start = false;
				i = 0;
				morse_symbol = getMorseCode(string[i]);	// get the next morse symbol
				if (morse_symbol != 0) {
					if (IS_DAH(morse_symbol)) {
						timer_start(&tmr, DAH_LENGTH);
					} else {
						timer_start(&tmr, DIT_LENGTH);
					}
					morse_symbol = morse_symbol>>1;
					BUZZER_PORT |= (1<<BUZZER_PIN);
					state = BEEP;
				}
			}
			break;

		case BEEP:
			if (timer_is_expired(&tmr)) {
				if (morse_symbol == 1) {				// 1 means the character is done
					i++;								// move forward in the string
					if (string[i] == '\0') {
						BUZZER_PORT &= ~(1<<BUZZER_PIN);
						state = IDLE;
					} else {
						morse_symbol = getMorseCode(string[i]);				// get the next morse symbol
						if (morse_symbol == 0xFF) {							// Detecting spaces
							timer_start(&tmr, PAUSE_WORD_TIME);				// Space detected -> pause between words
							i++;											// move to next character instantly
							morse_symbol = getMorseCode(string[i]);
						} else {
							timer_start(&tmr, PAUSE_CHARACTER_TIME);		// pause between characters
						}

						BUZZER_PORT &= ~(1<<BUZZER_PIN);
						state = PAUSE;
					}
				} else {	// character is not yet done
					timer_start(&tmr, PAUSE_SYMBOL_TIME);				// pause between morse symbols

					BUZZER_PORT &= ~(1<<BUZZER_PIN);
					state = PAUSE;
				}
			}
			break;

		case PAUSE:
			if (string[i] == '\0') {
				BUZZER_PORT &= ~(1<<BUZZER_PIN);
				state = IDLE;
			} else {
				if (timer_is_expired(&tmr)) {
					if (IS_DAH(morse_symbol)) {
						timer_start(&tmr, DAH_LENGTH);
					} else {
						timer_start(&tmr, DIT_LENGTH);
					}
					morse_symbol = morse_symbol>>1;
					BUZZER_PORT |= (1<<BUZZER_PIN);
					state = BEEP;
				}
			}
			break;
	}
}
