#ifndef CONFIG_H_
#define CONFIG_H_

#define REED_PORT   PORTB
#define REED_PIN    PB1

#define BUZZER_PORT PORTB
#define BUZZER_PIN  PB3

#define SERVO_PORT  PORTB
#define SERVO_PIN   PB2

#define LED_PORT        PORTC
#define LED_LVL         PC0
#define LED_KB          PC1

#define NAIL_DDR    DDRC
#define NAIL_PORT   PORTC
#define NAIL_PIN    PINC
#define NAIL1_PIN    PC2
#define NAIL2_PIN    PC4

#define LVL_LED_OFF_LONG_TIME   75
#define LVL_LED_OFF_SHORT_TIME  20
#define LVL_LED_ON_TIME         20

#define T1_LOAD_VAL 5000
#define SERVO_LOCKED_TIMER_VAL  375
#define SERVO_UNLOCKED_TIMER_VAL  600


#define KB_MAX_INP_LENGTH   21

#define KB_DEBOUNCE_TIME    10
#define KB_INPUT_TIMEOUT    200

#define KB_NOKEY            0xFF

#define KB_COL1         PD3
#define KB_COL1_PORT    PORTD
#define KB_COL2         PB5
#define KB_COL2_PORT    PORTB
#define KB_COL3         PD5
#define KB_COL3_PORT    PORTD

#define KB_ROW1     PB4
#define KB_ROW1_PIN PINB
#define KB_ROW2     PB0
#define KB_ROW2_PIN PINB
#define KB_ROW3     PD6
#define KB_ROW3_PIN PIND
#define KB_ROW4     PD4
#define KB_ROW4_PIN PIND


#define LVL1_SOLUTION_STRING    "54323" // = LIEBE
#define LVL2_SOLUTION_STRING    "24"    // 24 times "Fideralala"
#define LVL3_MORSE_STRING       "Lasset dunkles licht scheinen" // UV light hint
#define LVL4_MORSE_STRING       "a = 17"
#define LVL3_SOLUTION_STRING    "144"   // TODO

// TODO make cheatcodes unique and without pattern
#define CHEATCODE_LVL1      "#187**"
#define CHEATCODE_LVL2      "#152**"
#define CHEATCODE_LVL3      "#255**"
#define CHEATCODE_LVL4      "#772**"
#define CHEATCODE_LVL5      "#879**"
#define CHEATCODE_FINISH    "#574**"
#define CHEATCODE_LOCK      "#881**"
#define CHEATCODE_UNLOCK    "#882**"

#endif /* CONFIG_H_ */
