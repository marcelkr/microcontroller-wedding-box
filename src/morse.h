#ifndef _MORSE_H_
#define _MORSE_H_

#include <stdint.h>

extern void morse_process(void);
extern void morse_string(char* string_p);

#define DIT_LENGTH              25   // Length of a dit in milliseconds
#define DAH_LENGTH              3*DIT_LENGTH		// length of dah is defined as 3 times the length of dit
#define PAUSE_SYMBOL_TIME       DIT_LENGTH
#define PAUSE_CHARACTER_TIME    DAH_LENGTH
#define PAUSE_WORD_TIME         7*DIT_LENGTH	// time between two words is defined as 7 times the length of dit

#endif /* _MORSE_H_ */
