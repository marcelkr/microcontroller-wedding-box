/*
 * timer.c
 *
 *  Created on: 09.09.2014
 *      Author: marcel
 */

#include "timer.h"

volatile uint32_t timer_counter = 0;

/**
 * @brief Starts the given timer
 *
 * @param [in] soft_timer_t* The timer to be started
 */
void timer_start(soft_timer_t *timer, uint32_t timespan) {
	timer->timespan = timespan;
	timer->start_value = timer_counter;
	timer->running = true;
}

bool timer_is_running(soft_timer_t *timer) {
	return timer->running;
}


void timer_stop(soft_timer_t *timer) {
	timer->running = false;
}

/**
 * @brief Checks if the given timer is expired
 *
 * @param [in] soft_timer_t* The timer to be checked
 */
bool timer_is_expired(soft_timer_t *timer) {
	if(timer->start_value + timer->timespan <= timer_counter) {
		return true;
	} else {
		return false;
	}
}
