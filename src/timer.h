/*
 * timer.h
 *
 *  Created on: 09.09.2014
 *      Author: marcel
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <stdint.h>
#include <stdbool.h>

extern volatile uint32_t timer_counter;

typedef struct {
	uint32_t start_value; // TODO: Consider changing to 16 bit variables and running the counter at 0,1s ticks (results in 109 minutes max runtime) - No higher resolution needed anyway
	uint32_t timespan;
	bool running;
} soft_timer_t;

void timer_start(soft_timer_t *timer, uint32_t timespan);
void timer_stop(soft_timer_t *timer);
bool timer_is_running(soft_timer_t *timer);
bool timer_is_expired(soft_timer_t *timer);

#endif /* TIMER_H_ */
