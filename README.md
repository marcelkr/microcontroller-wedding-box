# Microcontrolled Wedding Present Box
 
![](images/box_4.jpg)

This was a wedding present to good friends of mine. It's a locked box which has a painted riddle on each side. To unlock it, the riddles have to be solved. To solve a riddle, either the keypad is used, a wooden heart is held to the box or electric connections have to be made via cables.

## Quick facts
- The box is controlled by an [Adafruit Pro Trinket](https://www.adafruit.com/product/2000).
- The locking mechanism is built with a servo.
- The battery holder is fitted below the box and user accessible to prevent locking the box in case the batteries go dead.
- A LED is showing which level you're in. 
- A buzzer is used for morse code.

## The riddles
These are the individual riddles painted on each side:

1. Guess the word LIEBE (german for love)
2. Count the times the word 'fideralala' is mentioned in the song linked to via the QR code. 
3. Do some ominous calculations which I can't remember anymore. As far as I can remember, `f` is the number of 'fideralalas' from the previous riddle. There's also a number written to the box with a UV pen. Therefore the hint morse hint 'let dark light shine'. The solution is `144`, which opens the padlock and reveals a wooden heart and some loose cables.
4. With the results of the calculations and the cables, numbered nails on the back of the box have to be connected properly.
5. The heart found in the locked compartment must be held near the heart at the front of the box. The reed 'sensor' registers the magnet built in the wooden heart. The box unlocks.

## The code
There's some options that can be changed in `config.h`. Before compiling, adapt the Makefile to suit your controller.

Compilation is done by: 
- Changing into `src/`
- Executing `make`

Putting together the microcontrollerboard and code was basically an all-nighter. Don't judge me the code quality :D
